# Kubernetes


## Getting started

**Main commands**

```shell script
kubectl get nodes
kubectl get pods
kubectl get pods -A

kubectl get services

kubectl create -h
```

Available Commands:
  clusterrole         Create a ClusterRole.
  clusterrolebinding  Create a ClusterRoleBinding for a particular ClusterRole
  configmap           Create a configmap from a local file, directory or literal value
  cronjob             Create a cronjob with the specified name.
  deployment          Create a deployment with the specified name.
  ingress             Create an ingress with the specified name.
  job                 Create a job with the specified name.
  namespace           Create a namespace with the specified name
  poddisruptionbudget Create a pod disruption budget with the specified name.
  priorityclass       Create a priorityclass with the specified name.
  quota               Create a quota with the specified name.
  role                Create a role with single rule.
  rolebinding         Create a RoleBinding for a particular Role or ClusterRole
  secret              Create a secret using specified subcommand
  service             Create a service using specified subcommand.
  serviceaccount      Create a service account with the specified name

1. Deployment.app
- Creating deployment
```shell script
kubectl create deployment NAME --image=image --dry-run options

kubectl create deployment nginx-dep --image=nginx

kubectl create deployment nginx-dep --image=nginx
deployment.apps/nginx-dep created
```

- Getting deployments
```shell script
ale@lap1:~$ kubectl get deployments.apps
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
nginx-dep   1/1     1            1           21s
```

- Getting pods
```shell script
ale@lap1:~$ kubectl get pods
NAME                        READY   STATUS    RESTARTS   AGE
nginx-dep-5c5477cb4-bgx4c   1/1     Running   0          37s
```

- Getting replicasets.apps
```shell script
ale@lap1:~$ kubectl get replicasets.apps
NAME                  DESIRED   CURRENT   READY   AGE
nginx-dep-5c5477cb4   1         1         1       49s
```

- Editting deployments.apps
```shell script
kubectl edit deployments.apps  nginx-dep
deployment.apps/nginx-dep edited

kubectl get deployments.apps
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
nginx-dep   1/1     1            1           4m54s

kubectl get pods
NAME                         READY   STATUS              RESTARTS   AGE
nginx-dep-5c5477cb4-bgx4c    1/1     Running             0          5m
nginx-dep-6f6cb5d9b8-h87b7   0/1     ContainerCreating   0          11s

kubectl get replicasets.apps
NAME                   DESIRED   CURRENT   READY   AGE
nginx-dep-5c5477cb4    0         0         0       5m8s
nginx-dep-6f6cb5d9b8   1         1         1       19s
```

## Debugin Pods

- Logging Pods
```shell script
ale@lap1:~$ kubectl logs nginx-dep-6f6cb5d9b8-h87b7
ale@lap1:~$ kubectl create deployment mongodb --image=mongo
deployment.apps/mongodb created

ale@lap1:~$ kubectl get pods
NAME                         READY   STATUS              RESTARTS   AGE
mongodb-756bc84fb6-xjtcx     0/1     ContainerCreating   0          7s
nginx-dep-6f6cb5d9b8-h87b7   1/1     Running             0          2m33s

ale@lap1:~$ kubectl logs mongodb-756bc84fb6-xjtcx
Error from server (BadRequest): container "mongo" in pod "mongodb-756bc84fb6-xjtcx" is waiting to start: ContainerCreating
```

mongo
mysql

- Executting Commands
```shell script
kubectl exec -it nginx -- bin/bash

kubectl create deployment name image option1 option2 option3
```shell


## Apply command
```shell script
kubectl apply -f config-file.yaml

touch nginx.yaml
nano nginx.yaml
```

```shell script
--- 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.16
        ports:
        - containerPort: 80

---
```

```shell script
kubectl apply -f nginx.yaml
deployment.apps/nginx-deployment created
```


```shell script
replicas: 2

kubectl apply -f nginx.yaml
deployment.apps/nginx-deployment configured

kubectl get deployments.apps
kubectl get pods
```

```shell script
---
apiVersion: v1
kind: Service
metadata:
  labels:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
    - name: "p80"
      port: 80
      targetPort: 8080
      protocol: TCP
```


```shell script
kubectl get pods
kubectl get service
```


```shell script
kubectl get svc
NAME            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
kubernetes      ClusterIP   10.96.0.1        <none>        443/TCP   3d6h
nginx-service   ClusterIP   10.107.100.113   <none>        80/TCP    17s
```


```shell script
kubectl describe service nginx-service
Name:              nginx-service
Namespace:         default
Labels:            <none>
Annotations:       <none>
Selector:          app=nginx
Type:              ClusterIP
IP Families:       <none>
IP:                10.107.100.113
IPs:               10.107.100.113
Port:              p80  80/TCP
TargetPort:        8080/TCP
Endpoints:         10.244.160.207:8080,10.244.160.208:8080
Session Affinity:  None
Events:            <none>
```


```shell script
kubectl get pods -o wide
NAME                               READY   STATUS             RESTARTS   AGE     IP               NODE   NOMINATED NODE   READINESS GATES
hello-5b464657d4-tslr5             0/1     ImagePullBackOff   28         4h34m   10.244.160.204   lap1   <none>           <none>
nginx-dep-6f6cb5d9b8-h87b7         1/1     Running            0          4h51m   10.244.160.198   lap1   <none>           <none>
nginx-deployment-f4b7bbcbc-cfh8c   1/1     Running            0          7m36s   10.244.160.208   lap1   <none>           <none>
nginx-deployment-f4b7bbcbc-djng5   1/1     Running            0          7m43s   10.244.160.207   lap1   <none>           <none>
python-dep-b8d696696-gvssf         0/1     ImagePullBackOff   30         4h44m   10.244.160.201   lap1   <none>           <none>
ubu-74c686978b-xwn5n               0/1     ImagePullBackOff   28         4h37m   10.244.160.203   lap1   <none>           <none>
```


```shell script
kubectl get deployments.apps nginx-deployment -o yaml
kubectl get deployments.apps nginx-deployment -o yaml > nginx-deployment-result.yaml
```

## Artifact Hub for Helm deployment

- https://artifacthub.io/
- https://artifacthub.io/packages/helm/bitnami/postgresql

